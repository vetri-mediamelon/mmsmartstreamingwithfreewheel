# MMSmartStreamingWithFreewheel

[![CI Status](https://img.shields.io/travis/Vetri/MMSmartStreamingWithFreewheel.svg?style=flat)](https://travis-ci.org/Vetri/MMSmartStreamingWithFreewheel)
[![Version](https://img.shields.io/cocoapods/v/MMSmartStreamingWithFreewheel.svg?style=flat)](https://cocoapods.org/pods/MMSmartStreamingWithFreewheel)
[![License](https://img.shields.io/cocoapods/l/MMSmartStreamingWithFreewheel.svg?style=flat)](https://cocoapods.org/pods/MMSmartStreamingWithFreewheel)
[![Platform](https://img.shields.io/cocoapods/p/MMSmartStreamingWithFreewheel.svg?style=flat)](https://cocoapods.org/pods/MMSmartStreamingWithFreewheel)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

MMSmartStreamingWithFreewheel is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'MMSmartStreamingWithFreewheel'
```

## Author

Vetri, vetri@mediamelon.com

## License

MMSmartStreamingWithFreewheel is available under the MIT license. See the LICENSE file for more info.
