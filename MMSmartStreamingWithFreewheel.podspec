#
# Be sure to run `pod lib lint MMSmartStreamingWithFreewheel.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'MMSmartStreamingWithFreewheel'
  s.version          = '0.1.1'
  s.summary          = 'The MediaMelon Player SDK Provides SmartSight Analytics and QBR SmartStreaming.'
  s.description      = 'The MediaMelon Player SDK adds SmartSight Analytics and QBR SmartStreaming capability to any media player and is available for all ABR media players.'
  s.homepage         = 'https://bitbucket.org/vetri-mediamelon/mmsmartstreamingwithfreewheel/'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Vetri' => 'vetri@mediamelon.com' }
  s.source           = { :git => 'https://bitbucket.org/vetri-mediamelon/mmsmartstreamingwithfreewheel', :tag => '0.1.1' }
  s.swift_version    = '4.0'
  s.ios.deployment_target = '9.0'
  s.tvos.deployment_target = '9.0'
  s.source_files = 'MMSmartStreamingWithFreewheel/Classes/Common/**/*.{h,swift}'
  s.ios.source_files = 'MMSmartStreamingWithFreewheel/Classes/iOS/*.{h,swift}'
  s.tvos.source_files = 'MMSmartStreamingWithFreewheel/Classes/tvOS/*.{h,swift}'
  s.frameworks = 'UIKit', 'AVFoundation', 'AdSupport', 'CoreLocation', 'CoreGraphics', 'SystemConfiguration'
  s.ios.frameworks = 'CoreTelephony', 'Messages', 'MessagesUI', 'WebKit'
  s.ios.vendored_libraries = 'MMSmartStreamingWithFreewheel/Classes/iOS/libmmsmartstreamer.a'
  s.tvos.vendored_libraries = 'MMSmartStreamingWithFreewheel/Classes/tvOS/libmmsmartstreaming-tvos.a'
  s.libraries = 'stdc++', 'xml2'
  s.public_header_files = 'MMSmartStreamingWithFreewheel/Classes/Common/MMSmartStreaming/**/*.h' , 'MMSmartStreamingWithFreewheel/Classes/Common/ReachabilityMM/*.h'
  s.static_framework = true
  s.xcconfig = { 'FRAMEWORK_SEARCH_PATHS' => '$(SRCROOT)/MMSmartStreamingWithFreewheel', 'OTHER_LDFLAGS' => '-ObjC', 'DEFINE_MODULES' => 'YES', 'VALID_ARCHS' => 'x86_64 arm64 amrv7', 'ARCHS' => 'x86_64 arm64' }
  s.ios.pod_target_xcconfig = { 'VALID_ARCHS' => 'x86_64 arm64 amrv7', 'ARCHS' => 'x86_64 arm64' }
  s.pod_target_xcconfig = {'VALID_ARCHS' => 'x86_64 arm64 amrv7', 'ARCHS' => 'x86_64 arm64'}
  s.dependency 'freewheel'
end
